# Sikolia-Api-Endpoints
The application provides api endpoints for consumption. It uses a database with a unique identifier (the email) to store user data associated with every email. It receives information sent from the user to register and log in the user.

# How to run the application
**Assumption: Your operating system should be ubuntu 16.04.**
Ensure you have chrome and python3 installed. Install postman in your chrome applications. For details of how to do this follow this [link](/"https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop?hl=en").
* Clone this repository.
* Navigate into the folder called *AndelaCohort28-LevelUP-WeekOne*
* Open the terminal inside this repository by right clicking anywhere inside the folder and selecting open terminal
* Activate your virtual environment by follwing the tutorial [here](/"https://docs.python.org/3/tutorial/venv.html").
* Type ```pip install -r requirements.txt``` on the terminal and press enter.


Create a collection on postman and store the following requests in the collection.
## Register User
* On postman click body and select the raw radio button. 
* Ensure the application is set to JSON(application/json). 
* Ensure the type of request is set to post. 
* Enter the link: ```http://127.0.0.1:5000/register``` on postman.
* Enter the following information in the body(type raw and application/json) and save before sending the request.
```{"firstname":"Wycliffe","lastname": "Sikolia","email": "sikolia.wycliffe@gmail.com", "password":"password","password2": "password"}```

## Login
Repeat the procedure above using the information below.
* request = POST
* link = ```http://127.0.0.1:5000/login```
* body = ```{"email": "sikolia.wycliffe@gmail.com", "password":"password"}```

## Post Comment
Repeat the procedure above using the information below.
* request = POST
* link = ```http://127.0.0.1:5000/post-comment```
* body = ```{"email":"sikolia.wycliffe@gmail.com","comment": ["Hurray! I have just made my first comment", "Not fun! The second comment is not much fun."]}```


## View Comments
Repeat the procedure above using the information below.
* request = GET
* link = ```http://127.0.0.1:5000/comment```
* Do not put anything in the body of the request.


## View User Details
Repeat the procedure above using the information below.
* request = GET
* link = ```http://127.0.0.1:5000/account```
* Do not put anything in the body of the request.


## Delete Comment
Repeat the procedure above using the information below.
* request = DELETE
* link = ```http://127.0.0.1:5000/comment/0```
* Do not put anything in the body of the request.


**Tada!!** You have just learned how to use Sikolia-Api-Endpoints
# Author
Wycliffe Atswenje Sikolia
